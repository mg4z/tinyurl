<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Tiny Url</title>

<link rel="stylesheet" type="text/css" href="css/style.css">

<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="/css/bootstrap.min.css" >

<!-- Optional theme -->
<link rel="stylesheet" href="/css/bootstrap-theme.min.css" >

</head>

<body>
<div class="container" >
  <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12">
    <div class="row">
      <h2>Enter your url</h2>
      <form  name="geturl" id="geturl" action="#">
        <input type="text"   id="url" name="url" class="input-lg col-lg-10 col-md-10  col-sm-12 col-xs-12">
        <input type="submit" id="send" name="submit" value="get" class="btn btn-lg btn-primary col-lg-2 col-md-2 col-sm-12 col-xs-12">
      </form>
    </div>
    <!-- end row --> 
    <div class="row">
      <div id="loading"><img src="img/loading.gif" width="100" alt="loading"/></div>
      <div id="result" class="col-lg-10 col-md-10  col-sm-12 col-xs-12"></div>
    </div>
     <!-- end row --> 
  </div>
  
  <!-- end div --> 
</div>
<!-- end containrt --> 

<script src="js/jquery-3.1.0.min.js"></script> 
<script src="js/bootstrap.min.js" ></script> 
<script src="js/custom.js"></script>
</body>
</html>