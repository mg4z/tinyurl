<?php

require_once('config.php');

/// This function create som random character for tiny url

function genrateurlkey($len = 8)
{
    $string   = '';
    $chars    = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charslen = strlen($chars);
    for ($i = 0; $i < $len; $i++) {
        $string .= $chars[rand(0, $charslen - 1)];
    }
    return $string;
}


/// This function check if url is corect and worked.

function ExistsUrl($url = NULL)
{
    if ($url == NULL)
        return false;
    $is = curl_init($url);
    curl_setopt($is, CURLOPT_URL, $url);
    curl_setopt($is, CURLOPT_TIMEOUT, 60);
    curl_setopt($is, CURLOPT_CONNECTTIMEOUT, 60);
    curl_setopt($is, CURLOPT_FOLLOWLOCATION, 1);
    curl_setopt($is, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($is, CURLOPT_HEADER, 1);
    curl_setopt($is, CURLOPT_AUTOREFERER, 1);
    curl_setopt($is, CURLOPT_MAXREDIRS, 10);
    curl_setopt($is, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($is, CURLOPT_SSL_VERIFYPEER, 0);
    
    $data = curl_exec($is);
    $code = curl_getinfo($is, CURLINFO_HTTP_CODE);
    
    //curl_close($is); 
    if ($code == 200) {
        return true;
    } else {
        return false;
    }
}

function userip()
{
    
    if (array_key_exists('HTTP_X_FORWARDED_FOR', $_SERVER)) {
        $resultip = $_SERVER['HTTP_X_FORWARDED_FOR'];
    } elseif (array_key_exists('HTTP_CLIENT_IP', $_SERVER)) {
        $resultip = $_SERVER['HTTP_CLIENT_IP'];
    } else {
        $resultip = $_SERVER['REMOTE_ADDR'];
    }
    if (strstr($resultip, ',')) {
        $resultip = strtok($resultip, ',');
    }
    
    if (!preg_match("#^[0-9]{1,3}.[0-9]{1,3}.[0-9]{1,3}.[0-9]{1,3}#", $resultip)) {
        $resultip = $_SERVER["REMOTE_ADDR"];
    }
    
    return $resultip;
}


try {
    
    $org_url = trim($_POST['url']);
    
    // Hashing url for databse index.
	
	$hash_url = hash('sha1', $org_url);
	
	
    // get the new url and find it on databse
    
    $query = $db->prepare('SELECT * FROM list WHERE `hashurl`=? or `tinyurl` =? ');
    $query->bindParam(1, $hash_url, PDO::PARAM_STR);
    $query->bindParam(2, $org_url, PDO::PARAM_STR);
    $query->execute();
    $row = $query->fetch(PDO::FETCH_ASSOC);
    
    
    
    // check th url for it is valid and work.    
    if ($row) {
        $error = '';
        if ($errorcode = ExistsUrl($row['orgurl']) != true)
            $error = 'Your url is not exist.';
        else
        // Return the tiny url if it was created befor and avalibale in database.
            $tiny_url = $row['tinyurl'];
        
        
    } else {
        $error = '';
        
        // check if url is work then create tiny url and add it to database.
        
        if ($errorcode = ExistsUrl($org_url) != true) {
            $error = 'Your url is not exist.';
        } else {
            $tiny_url = $_SERVER['HTTP_HOST'] . '/' . genrateurlkey();
            $ip       = userip();
			
            $sql = "INSERT INTO list (orgurl, tinyurl,ip,hashurl) VALUES ('" . $org_url . "', '" . $tiny_url . "', '" . $ip . "', '" . $hash_url . "')";
            $db->exec($sql);
        }
        
    }
    
    // return the link of tiny url.
    
    // add http to url
    
    $parsit = parse_url($tiny_url);
    if (empty($parsit['scheme'])) {
        $tiny_url_href = 'https://' . ltrim($tiny_url, '/');
    }
    
    print ($error == '') ? "<a href='" . $tiny_url_href . "' target='_blank'>" . $tiny_url . "</a>" : $error;
    
    
}
catch (PDOException $e) {
    $e->getMessage();
}

$db = null;

?>