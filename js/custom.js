
;(function($) {
	
	 $('#result').hide();
	 $('#loading').hide();

	$("#geturl").submit(function(event) {
	
	if( $("#url").val().length<3 )
		{
			$( "#url" ).val('Enter your url');
			$( "#url" ).focus(function() {
			  $( this ).val( '' );
			});
		}
		else
		{
			
	  var dataString = $('#geturl').serialize();


	  $.ajax({
      type: "POST",
      url: "ajax.php",
      data: dataString,
	  beforeSend: function() { $('#loading').show(); },
      complete: function() { $('#loading').hide(); },
      success: function(html) {
		  		  
		if( html != '' )
		{
			$('#result').fadeOut(500);
			$('#result').empty();
			
			$("#result").fadeIn(1000, function () {
                       $('#result').append(html);
            });
			
			
		}
			
		
		},
		  error : function(XMLHttpRequest, textStatus, errorThrown) {
					alert('There was an error.');
					return false;
				}
		 });
	 
		}
		 event.preventDefault();
	});
		
	
})(jQuery);

