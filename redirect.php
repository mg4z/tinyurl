<?php

require_once('config.php');

try {
    $get_url = $_SERVER['HTTP_HOST'] . '/' . trim($_GET['url']);
    $query   = $db->prepare('SELECT id,orgurl FROM list WHERE tinyurl=?');
    $query->bindParam(1, $get_url, PDO::PARAM_STR);
    $query->execute();
    $row = $query->fetch(PDO::FETCH_ASSOC);
}

catch (PDOException $e) {
    $e->getMessage();
}

if ($row) {
    
    try {
        $query = $db->prepare("UPDATE list SET `clicks` = `clicks` + '1' WHERE `id`='" . $row['id'] . "'");
        $query->execute();
        
    }
    catch (PDOException $e) {
        $e->getMessage();
    }
    
    
    $url    = $row['orgurl'];
    $parsit = parse_url($url);
    if (empty($parsit['scheme'])) {
        $url = 'http://' . ltrim($url, '/');
    }
    
    header('Location: ' . $url);
} else {
    header('Location: /');
}



$db = null;

?>