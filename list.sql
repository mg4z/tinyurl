/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50623
Source Host           : localhost:3306
Source Database       : ratanet_tinyurl

Target Server Type    : MYSQL
Target Server Version : 50623
File Encoding         : 65001

Date: 2016-09-22 17:11:22
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for list
-- ----------------------------
DROP TABLE IF EXISTS `list`;
CREATE TABLE `list` (
  `orgurl` text COLLATE utf8_persian_ci,
  `tinyurl` varchar(50) COLLATE utf8_persian_ci DEFAULT NULL,
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `creatdate` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `clicks` bigint(255) DEFAULT '0',
  `ip` varchar(50) COLLATE utf8_persian_ci DEFAULT NULL,
  `hashurl` varchar(255) COLLATE utf8_persian_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `tiny` (`tinyurl`) USING BTREE,
  KEY `hash` (`hashurl`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=55 DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci;
